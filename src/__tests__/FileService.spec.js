const path = require('path');
const fs = require('fs');
const { readFile, writeFile, bestRoute } = require('../services/File');

const originalFile = path.resolve(__dirname, '__mocks__', 'input-file.csv');
const bkpFile = path.resolve(__dirname, '__mocks__', 'input-file-bkp.csv');

beforeAll(() => {
  fs.copyFileSync(originalFile, bkpFile);
  process.argv[2] = originalFile;
});

afterAll(() => {
  fs.unlinkSync(originalFile);
  fs.copyFileSync(bkpFile, originalFile);
});

describe('FileService Tests', () => {
  it('should read the file', async () => {
    const routes = await readFile();

    expect(routes).toEqual([
      ['GRU', 'BRC', '10'],
      ['BRC', 'SCL', '5'],
      ['GRU', 'BRC', '75'],
    ]);
  });

  it('should write a new line in a file', async () => {
    writeFile('BRC,GRU,10');

    const routes = await readFile();

    expect(routes).toEqual([
      ['GRU', 'BRC', '10'],
      ['BRC', 'SCL', '5'],
      ['GRU', 'BRC', '75'],
      ['BRC', 'GRU', '10'],
    ]);
  });

  it('should return the best route', async () => {
    const result = await bestRoute('GRU', 'BRC');

    expect(result).toEqual('GRU - BRC > $10');
  });

  it('should return no available route', async () => {
    const result = await bestRoute('GRU', 'BLA');

    expect(result).toEqual('No Available Routes');
  });
});
