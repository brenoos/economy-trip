const { bestRoute, writeFile } = require('../../services/File');

class RouteController {
  async bestRoute(req, res) {
    const { start = '', end = '' } = req.query;

    const bestRouteResult = await bestRoute(start, end);

    return res.json({ bestRoute: bestRouteResult });
  }

  storeRoute(req, res) {
    const { route } = req.body;

    writeFile(route);

    return res.json({ message: 'Route added' });
  }
}

module.exports = new RouteController();
