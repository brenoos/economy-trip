const routes = require('express').Router();
const RouteController = require('./controllers/RouteController');

routes.get('/route', RouteController.bestRoute);
routes.post('/route', RouteController.storeRoute);

module.exports = routes;
