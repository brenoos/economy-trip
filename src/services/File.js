const fs = require('fs');
const csv = require('csv-parser');
const path = require('path');

function readFile() {
  const file =
    process.argv[2] || path.resolve(__dirname, '..', '..', 'input-file.csv');

  const readStream = fs.createReadStream(file);

  const finalData = [];
  return new Promise((resolve, reject) => {
    readStream
      .pipe(csv({ headers: false }))
      .on('data', (line) => {
        finalData.push(Object.values(line));
      })
      .on('end', () => {
        resolve(finalData);
      })
      .on('error', (err) => reject(err));
  });
}

function writeFile(route) {
  const file =
    process.argv[2] || path.resolve(__dirname, '..', '..', 'input-file.csv');

  fs.appendFileSync(file, `\n${route.toUpperCase()}`);
}

async function bestRoute(start = '', end = '') {
  const routes = await readFile();

  const availableRoutes = routes.filter(
    (route) =>
      route[0] === start.toUpperCase() &&
      route[route.length - 2] === end.toUpperCase()
  );

  if (availableRoutes.length <= 0) {
    return 'No Available Routes';
  }

  const cheapestRoute = availableRoutes.reduce((cheapRoute, currentRoute) => {
    return Number(currentRoute[currentRoute.length - 1]) <
      Number(cheapRoute[cheapRoute.length - 1])
      ? currentRoute
      : cheapRoute;
  });

  const price = cheapestRoute.pop();

  return `${cheapestRoute.join(' - ')} > $${price}`;
}

module.exports = { readFile, bestRoute, writeFile };
