const inquirer = require('inquirer');
const { bestRoute } = require('./src/services/File');

async function main() {
  const { desiredRoute } = await inquirer.prompt({
    type: 'input',
    name: 'desiredRoute',
    message: 'please enter the route:',
  });

  const [start, end] = desiredRoute.trim().split('-');

  const bestRouteResult = await bestRoute(start, end);

  console.log('best route: ', bestRouteResult);
}

main();
