# Rota de Viagem

Para rodar o software você deve ter o Node e o yarn instalados.

após isso instala os pacotes:

```shell
$ yarn install
```

###### Para rodar em modo CLI

execute:

```shell
$ node cli.js input-file.csv
```

_Obs: Caso não passe o arquivo, ele será automaticamente lido do caminho padrão_

Após isso irá aparecer o input e é só digitar no seguinte formato: GRU-CDG.

###### Para a api rest

```shell
$ yarn start
```

a api estará rodando em http://localhost:3000

a rota disponível é `/route`

###### Para Consultar a melhor rota

Consulte a rota via GET passando por query string os parametros: `start` e `end`

exemplo:

`http://localhost:3000/route?start=gru&end=cdg`

###### Para Cadastrar nova rota

Utilize o metodo POST passando um body em formato JSON com uma chave `route`

exemplo

```json
{
  "route": "GRU,SLC,BRC,50"
}
```

###### Para rodar os testes unitários

execute

```shell
$ yarn test
```
